﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GetDataClient
{
    public class Client
    {   public int Id { get; set; }
        public string Name { get; set; }
        public DateTime BirthDate { get; set; }
        public string PhoneNumber { get; set; }
        public string Address { get; set; }
        public string SocialNumber { get; set; }
    }

    public class ClientContext : DbContext
    {
        public ClientContext() :
            base("ClientsData")
        { }

        public DbSet<Client> Clients { get; set; }
    }
}
