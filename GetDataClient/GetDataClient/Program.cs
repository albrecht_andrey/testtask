﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Excel = Microsoft.Office.Interop.Excel;

namespace GetDataClient
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("SocialNumber:");
            string seachSN = Console.ReadLine();
            if (seachSN.Length == 0)
            {
                seachSN = "12345678901234";
            }
            using (ClientContext db = new ClientContext())
            {
                var clients = db.Clients.Where(p => p.SocialNumber == seachSN);
                foreach (Client client in clients)
                {
                    Console.WriteLine($"{client.Name} ({client.SocialNumber})");
                    ToExcel(client.Id, client.Name, client.BirthDate, client.PhoneNumber, 
                        client.Address, client.SocialNumber);
                }
            }
        }

        private static void ToExcel(int id, string name, DateTime date, string phone, 
            string address, string socialN)
        {
            Excel.Application xlApp = new Excel.Application();
            Excel.Workbook xlWB;
            Excel.Worksheet xlSht;

            FileInfo file = new FileInfo(@"Template\example.xlsx");
            try
            {
                xlWB = xlApp.Workbooks.Open(file.FullName);
                xlSht = xlWB.Worksheets["Лист1"];

                xlSht.Cells.Replace("[ID]", id.ToString(),Excel.XlLookAt.xlPart, Excel.XlSearchOrder.xlByColumns);
                xlSht.Cells.Replace("[Name]", name, Excel.XlLookAt.xlPart, Excel.XlSearchOrder.xlByColumns);
                xlSht.Cells.Replace("[BirthDate]", date, Excel.XlLookAt.xlPart, Excel.XlSearchOrder.xlByColumns);
                xlSht.Cells.Replace("[PhoneNumber]", phone, Excel.XlLookAt.xlPart, Excel.XlSearchOrder.xlByColumns);
                xlSht.Cells.Replace("[Address]", address, Excel.XlLookAt.xlPart, Excel.XlSearchOrder.xlByColumns);
                xlSht.Cells.Replace("[SocialNumber]", socialN, Excel.XlLookAt.xlPart, Excel.XlSearchOrder.xlByColumns);

                DirectoryInfo directorySave = new DirectoryInfo("Result");
                xlWB.SaveAs($"{directorySave.FullName}\\{DateTime.Now.ToShortDateString()}.xlsx");
            }
            finally
            {
                xlApp.Quit();
            }
        }
    }
}
